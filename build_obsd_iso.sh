#!/bin/sh
# build openbsd custom iso
# require genisoimage

. ./obsdiso.conf

echo "---"
echo "* Rebuilding iso"
#mkisofs -r -no-emul-boot -b ${VERSION}/${ARCH}/cdbr -c boot.catalog -o ${PWD}/OpenBSD.iso .
genisoimage -r -no-emul-boot -b ${VERSION}/${ARCH}/cdbr -c boot.catalog -o ${CWD}/RetroBSD.iso ${NAME} 
