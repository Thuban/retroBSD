#!/bin/sh
# Auteur :      thuban <thuban@yeuxdelibad.net>
# licence :     GNU General Public Licence v3

# Description : Download and extract openbsd iso.
# require curl

. ./obsdiso.conf

#wget --passive-ftp --reject "*iso" ftp://ftp.fr.openbsd.org/pub/OpenBSD/5.9/amd64/* 
echo "---"
echo "* Downloading OpenBSD iso"
if [ ! -e ${NAME}.iso ]; then
    curl -# -C - -o ${NAME}.iso "http://ftp.fr.openbsd.org/pub/OpenBSD/${VERSION}/${ARCH}/install${V1}${V2}.iso"
fi

echo "---"
echo "* Extracting iso..."
mkdir -p loopdir
sudo mount -o loop ${NAME}.iso loopdir
cp -rv loopdir/ ./${NAME}
chown -R 1000:1000 ./${NAME}/
chmod -R +w ./${NAME}/
sudo umount loopdir/
rm -r loopdir/

mkdir -p site/etc
touch site/install.site
chmod 755 site/install.site
touch site/etc/rc.firsttime
chmod 755 site/etc/rc.firsttime

echo "* Now edit the files in ${NAME}"

exit 0

