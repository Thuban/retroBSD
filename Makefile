# N2O4B2SD makefile :: thuban <thuban@yeuxdelibad.net>


all:
ifeq ($(ARCH),)
	@echo "Usage: as root"
	@echo "make ARCH=<arch>		: build NoNoobOnOBSD "
	@echo "make clean			: clean up build directories"
	@echo "make cleanfull		: clean up build directories completely"
else
	@echo "Building OpenBSD iso - $(ARCH)"
	@echo "-------------------------"
	@./prepare_iso.sh $(ARCH)
	@./build_site.sh $(ARCH)
	@./build_obsd_iso.sh $(ARCH)
endif

prepare:
	@echo "Setting up build environment"
	@echo "-------------------------"
	@./prepare_iso.sh $(ARCH)

clean:
	@echo "Cleaning build environment"
	@echo "-------------------------"
	@rm -vrf retroBSD.iso
	@rm -vrf install*-*

cleanfull:
	@echo "Cleaning entire build environment"
	@echo "-------------------------"
	@rm -vrf retroBSD.iso
	@rm -vrf install*

